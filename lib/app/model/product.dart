// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:typed_data';

class Product {
  Product({
    this.id,
    this.item_name,
    this.img,
    this.item_id,
    this.item_code,
    this.type,
    this.minute,
    this.location,
    this.hrg_jual,
    this.hrg_beli,
    this.sub_total,
    this.kategori,
    this.qty = 0,
  });

  int? id;
  String? item_name;
  Uint8List? img;
  String? item_id;
  String? item_code;
  String? type;
  int? minute;
  String? location;
  String? hrg_jual;
  String? hrg_beli;
  int? sub_total;
  String? kategori;
  int? qty;

  Product.fromMap(Map map) {
    id = map[id];
    item_name = map[item_name];
    img = map[img];
    item_id = map[item_id];
    item_code = map[item_code];
    type = map[type];
    minute = map[minute];
    location = map[location];
    hrg_jual = map[hrg_jual];
    hrg_beli = map[hrg_beli];
    sub_total = map[sub_total];
    kategori = map[kategori];
  }

  Map<String, dynamic> toMap() => {
        "id": id,
        "item_name": item_name,
        "img": img,
        "item_id": item_id,
        "item_code": item_code,
        "type": type,
        "minute": minute,
        "location": location,
        "hrg_jual": hrg_jual,
        "hrg_beli": hrg_beli,
        "sub_total": sub_total,
        "kategori": kategori,
      };
  @override
  String toString() {
    // TODO: implement toString
    return '{type:$type,kategori:$kategori}';
  }
}
