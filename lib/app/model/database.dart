import 'package:diyo/app/model/product.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'package:path/path.dart';

class DbManager {
  Database? _database;

  Future openDb() async {
    _database = await openDatabase(join(await getDatabasesPath(), "diyo.db"),
        version: 2, onCreate: (Database db, int version) async {
      await db.execute(
        """CREATE TABLE product(id INTEGER PRIMARY KEY autoincrement, 
        item_name TEXT,
        img BLOB, 
        item_id TEXT, 
        item_code TEXT, 
        type TEXT, 
        minute INTEGER, 
        location TEXT, 
        hrg_jual TEXT, 
        hrg_beli TEXT, 
        sub_total INTEGER, 
        kategori TEXT
        )""",
      );
    });
    return _database;
  }
   Future insertproduct(
      Product product, String itemCode) async {
    await openDb();
    return await _database!
        .insert('product', product.toMap());
  }
    Future<List<Product>> getProduct() async {
    await openDb();
    final List<Map<String, dynamic>> maps = await _database!
        .query('product');

    return List.generate(maps.length, (i) {
      return Product(
          id: maps[i]['id'],
          item_name: maps[i]['item_name'],
          img: maps[i]['img'],
          item_id: maps[i]['item_id'],
          item_code: maps[i]['item_code'],
          hrg_jual: maps[i]['hrg_jual'],
          hrg_beli: maps[i]['hrg_beli'],
          sub_total: maps[i]['sub_total'],
          location: maps[i]['location'],
          minute: maps[i]['minute'],
          type: maps[i]['type'],
          qty: maps[i]['qty'],
          kategori: maps[i]['kategori']);
    });
  }
}
