import 'package:flutter/material.dart';

class AssetColors{
   static Color primary=const Color(0xffF1523F);
   static Color inActive=const Color(0xffD3DCE0);
   static Color background=const Color(0xffFAFAFA);
}