import 'dart:io';
import 'dart:typed_data';

import 'package:diyo/app/model/assets_color.dart';
import 'package:diyo/app/screens/menu/controller/add_menu_controller.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'dart:isolate';
import 'package:google_fonts/google_fonts.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';

class AddMenu extends GetView<AddMenuController> {
  final _key = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, designSize: const Size(360, 720));
    return GetBuilder<AddMenuController>(
        init: AddMenuController(),
        builder: (controller) {
          return Obx(() => Scaffold(  appBar: AppBar(
                  backgroundColor: AssetColors.primary,
                  title: Text("DIYO"),
                ),
            body: SingleChildScrollView(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 16.h,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 16.w),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                      color: Colors.white,
                                      width: 130.w,
                                      height: 130.h,
                                      child: controller.path!.value != ''
                                          ? Image.file(
                                              File(controller.path!.value),
                                              height: 60,
                                            )
                                          : Container(
                                              decoration: const BoxDecoration(
                                                  image: DecorationImage(
                                                      image: AssetImage(
                                                          'assets/images/logo.png'))))),
                                  SizedBox(
                                    height: 8.h,
                                  ),
                                  Container(
                                    width: 130,
                                    height: 35,
                                    child: ElevatedButton(
                                      style: ButtonStyle(
                                          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                              RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          8.0.r),
                                                  side: const BorderSide(
                                                      color: Colors.red))),
                                          backgroundColor:
                                              MaterialStateProperty.all<Color>(
                                                  AssetColors.primary),
                                          textStyle:
                                              MaterialStateProperty.all<TextStyle>(
                                                  const TextStyle(
                                                      fontSize: 13,
                                                      fontWeight: FontWeight.bold))),
                                      onPressed: () async {
                                        //select Photo

                                        final ImagePicker _picker = ImagePicker();
                                        // Pick an image
                                        final XFile? res =
                                            await _picker.pickImage(
                                                source: ImageSource.gallery);

                                        controller.setImage(res!);
                                      },
                                      child: Text(
                                        "Select Photo",
                                        style: GoogleFonts.poppins(),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                width: 8.w,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(top: 8.h),
                                    child: Container(
                                      width: 190.w,
                                      child: TextFormField(
                                        maxLines: 1,
                                        validator: (e) {
                                          if (e!.isEmpty) {
                                            return "Masukan Nama Menu";
                                          }
                                        },
                                        controller:
                                            controller.nameProductController,
                                        decoration: InputDecoration(
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8.r),
                                            borderSide: BorderSide(
                                                color: AssetColors.primary,
                                                width: 0.5),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8.r),
                                            borderSide: BorderSide(
                                                color: AssetColors.primary,
                                                width: 0.5),
                                          ),
                                          hintText: 'Name Product',
                                          contentPadding: EdgeInsets.only(
                                              top: 0,
                                              bottom: 0,
                                              left: 8.w,
                                              right: 8.w),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 16.h),
                                    child: Container(
                                      width: 190.w,
                                      child: TextFormField(
                                        controller: controller.itemCodeController,
                                        maxLines: 1,
                                        validator: (e) {
                                          if (e!.isEmpty) {
                                            return "Masukan Item Code Menu";
                                          }
                                        },
                                        decoration: InputDecoration(
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8.r),
                                            borderSide: BorderSide(
                                                color: AssetColors.primary,
                                                width: 0.5),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8.r),
                                            borderSide: BorderSide(
                                                color: AssetColors.primary,
                                                width: 0.5),
                                          ),
                                          hintText: 'Item Code',
                                          contentPadding: EdgeInsets.only(
                                              top: 0,
                                              bottom: 0,
                                              left: 8.w,
                                              right: 8.w),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 16.h),
                                    child: Container(
                                      width: 190.w,
                                      child: TextFormField(
                                        controller:
                                            controller.typeProductController,
                                        maxLines: 1,
                                        validator: (e) {
                                          if (e!.isEmpty) {
                                            return "Masukan Type Menu";
                                          }
                                        },
                                        decoration: InputDecoration(
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8.r),
                                            borderSide: BorderSide(
                                                color: AssetColors.primary,
                                                width: 0.5),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8.r),
                                            borderSide: BorderSide(
                                                color: AssetColors.primary,
                                                width: 0.5),
                                          ),
                                          hintText: 'Type',
                                          contentPadding: EdgeInsets.only(
                                              top: 0,
                                              bottom: 0,
                                              left: 8.w,
                                              right: 8.w),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding:
                              EdgeInsets.only(top: 16.h, left: 16, right: 16),
                          child: Container(
                            width: 1.sw,
                            child: TextFormField(
                              controller: controller.timeController,
                              keyboardType: TextInputType.number,
                              maxLines: 1,
                              validator: (e) {
                                if (e!.isEmpty) {
                                  return "Masukan Waktu Masak Menu";
                                }
                              },
                              decoration: InputDecoration(
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8.r),
                                  borderSide: BorderSide(
                                      color: AssetColors.primary, width: 0.5),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8.r),
                                  borderSide: BorderSide(
                                      color: AssetColors.primary, width: 0.5),
                                ),
                                hintText: 'Waktu',
                                contentPadding: EdgeInsets.only(
                                    top: 0, bottom: 0, left: 8.w, right: 8.w),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding:
                              EdgeInsets.only(top: 16.h, left: 16, right: 16),
                          child: Container(
                            width: 1.sw,
                            child: TextFormField(
                              controller: controller.locationController,
                              maxLines: 1,
                              validator: (e) {
                                if (e!.isEmpty) {
                                  return "Masukan Nama Menu";
                                }
                              },
                              decoration: InputDecoration(
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8.r),
                                  borderSide: BorderSide(
                                      color: AssetColors.primary, width: 0.5),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8.r),
                                  borderSide: BorderSide(
                                      color: AssetColors.primary, width: 0.5),
                                ),
                                hintText: 'Lokasi',
                                contentPadding: EdgeInsets.only(
                                    top: 0, bottom: 0, left: 8.w, right: 8.w),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding:
                              EdgeInsets.only(top: 16.h, left: 16, right: 16),
                          child: Container(
                            width: 1.sw,
                            child: TextFormField(keyboardType: TextInputType.number,
                              controller: controller.purchasePriceController,
                              maxLines: 1,
                              validator: (e) {
                                if (e!.isEmpty) {
                                  return "Masukan Harga Beli ";
                                }
                              },
                              decoration: InputDecoration(
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8.r),
                                  borderSide: BorderSide(
                                      color: AssetColors.primary, width: 0.5),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8.r),
                                  borderSide: BorderSide(
                                      color: AssetColors.primary, width: 0.5),
                                ),
                                hintText: 'Harga Beli',
                                contentPadding: EdgeInsets.only(
                                    top: 0, bottom: 0, left: 8.w, right: 8.w),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding:
                              EdgeInsets.only(top: 16.h, left: 16, right: 16),
                          child: Container(
                            width: 1.sw,
                            child: TextFormField(keyboardType: TextInputType.number,
                              controller: controller.sellingPriceController,
                              maxLines: 1,
                              validator: (e) {
                                if (e!.isEmpty) {
                                  return "Masukan Harga Jual";
                                }
                              },
                              decoration: InputDecoration(
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8.r),
                                  borderSide: BorderSide(
                                      color: AssetColors.primary, width: 0.5),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8.r),
                                  borderSide: BorderSide(
                                      color: AssetColors.primary, width: 0.5),
                                ),
                                hintText: 'Harga Jual',
                                contentPadding: EdgeInsets.only(
                                    top: 0, bottom: 0, left: 8.w, right: 8.w),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding:
                              EdgeInsets.only(top: 16.h, left: 16, right: 16),
                          child: Container(
                            width: 1.sw,
                            child: TextFormField(
                              controller: controller.kategoriController,
                              maxLines: 1,
                              validator: (e) {
                                if (e!.isEmpty) {
                                  return "Masukan Nama Kategori";
                                }
                              },
                              decoration: InputDecoration(
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8.r),
                                  borderSide: BorderSide(
                                      color: AssetColors.primary, width: 0.5),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8.r),
                                  borderSide: BorderSide(
                                      color: AssetColors.primary, width: 0.5),
                                ),
                                hintText: 'Kategori',
                                contentPadding: EdgeInsets.only(
                                    top: 0, bottom: 0, left: 8.w, right: 8.w),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(16.0.sp),
                          child: Container(
                            width: 1.sw,
                            height: 35.h,
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  primary: const Color(0xffFB4E4E),
                                  textStyle: const TextStyle(
                                      fontSize: 13, fontWeight: FontWeight.bold)),
                              onPressed: () async {
                                controller.setProduct();
                              },
                              child: Text(
                                "Save Menu",
                                style: GoogleFonts.poppins(),
                              ),
                            ),
                          ),
                        ),
                      ]),
                ),
          ));
        });
  }
}
