import 'package:diyo/app/model/assets_color.dart';
import 'package:diyo/app/model/product.dart';
import 'package:diyo/app/screens/menu/views/add_menu.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../scanQRtable/views/scan_table.dart';
import '../controller/display_menu_controller.dart';

class DisplayMenu extends GetView<DisplayMenuController> {
  final _key = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, designSize: const Size(360, 720));
    return GetBuilder<DisplayMenuController>(
        init: DisplayMenuController(),
        builder: (controller) {
          return Obx(() => Container(
              padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 8.h),
              color: AssetColors.background,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Semua Restoran",
                        style: GoogleFonts.montserrat(
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            color: Colors.black87),
                      ),
                      TextButton(
                        onPressed: () {
                          Get.to(AddMenu());
                        },
                        style: ButtonStyle(
                          padding: MaterialStateProperty.all<EdgeInsets>(
                              EdgeInsets.zero),
                        ),
                        child: Text(
                          "Tambahkan Menu",
                          style: GoogleFonts.montserrat(
                              fontSize: 12,
                              fontWeight: FontWeight.w300,
                              color: AssetColors.primary),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: controller.listProduct!.length,
                        itemBuilder: (context, index) {
                          Product e = controller.listProduct![index];
                          return GestureDetector(
                            onTap: () {
                              Get.to(ScanQRTable());
                            },
                            child: Container(
                              padding: EdgeInsets.all(10.sp),
                              margin: EdgeInsets.symmetric(vertical: 4.h),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(8.r)),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(8.0),
                                    child: Image.memory(
                                      e.img!,
                                      width: 65.w,
                                      height: 65.h,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 8.w,
                                  ),
                                  Expanded(
                                    child: SizedBox(
                                      height: 65.h,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: [
                                          Text(
                                            e.item_name!,
                                            style: GoogleFonts.poppins(
                                              fontSize: 12.sp,
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                          Text(
                                            e.location!,
                                            style: GoogleFonts.poppins(
                                              fontSize: 12.sp,
                                              fontWeight: FontWeight.w300,
                                            ),
                                          ),
                                          Text(
                                            e.type!,
                                            style: GoogleFonts.poppins(
                                              fontSize: 12.sp,
                                              fontWeight: FontWeight.w400,
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.all(4.sp),
                                    decoration: BoxDecoration(
                                        color: AssetColors.inActive,
                                        borderRadius:
                                            BorderRadius.circular(4.r)),
                                    child: Row(children: [
                                      Icon(
                                        Icons.access_time_outlined,
                                        color: Colors.black54,
                                        size: 16,
                                      ),
                                      Text(
                                        "${e.minute} menit",
                                        style: GoogleFonts.montserrat(
                                            fontSize: 9.sp,
                                            color: Colors.black54),
                                      )
                                    ]),
                                  )
                                ],
                              ),
                            ),
                          );
                        }),
                  )
                ],
              )));
        });
  }
}
