import 'dart:io';

import 'package:diyo/app/model/database.dart';
import 'package:diyo/app/model/product.dart';
import 'package:diyo/app/screens/dashboard/views/home.dart';
import 'package:diyo/app/screens/menu/controller/display_menu_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:image_picker/image_picker.dart';

class AddMenuController extends GetxController {
  Rx<XFile?>? file = XFile('').obs;

  RxString? path = ''.obs;
  TextEditingController nameProductController = TextEditingController();
  TextEditingController itemCodeController = TextEditingController();
  TextEditingController typeProductController = TextEditingController();
  TextEditingController timeController = TextEditingController();
  TextEditingController locationController = TextEditingController();
  TextEditingController purchasePriceController = TextEditingController();
  TextEditingController sellingPriceController = TextEditingController();
  TextEditingController kategoriController = TextEditingController();
  setImage(XFile res) async {
    file!.value = res;
    path!.value = res.path;
  }

  setProduct() async {
    DbManager dbManager = DbManager();
    Product product = Product();
    product.item_name = nameProductController.text;
    product.img = await file!.value!.readAsBytes();
    product.item_code = itemCodeController.text;
    product.hrg_beli = purchasePriceController.text;
    product.hrg_jual = sellingPriceController.text;
    product.location = locationController.text;
    product.kategori = kategoriController.text;
    product.sub_total = int.parse(sellingPriceController.text);
    product.type = typeProductController.text;
    product.item_id = 1.toString();
    product.minute = int.parse(timeController.text);
    dbManager.insertproduct(product, product.item_code!);
    print(product.toString());
    Get.offAllNamed('/home');
    DisplayMenuController displayMenu = Get.put(DisplayMenuController());
    displayMenu.getProduct();
  }
}
