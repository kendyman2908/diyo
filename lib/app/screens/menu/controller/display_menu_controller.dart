import 'dart:typed_data';

import 'package:diyo/app/screens/order/views/order.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

import '../../../model/database.dart';
import '../../../model/product.dart';
import '../../checkout/views/checkout.dart';
import '../../scanQRtable/views/scan_table.dart';

class DisplayMenuController extends GetxController {
  RxList<Product>? listProduct = <Product>[].obs;
  RxList<Product>? product = <Product>[].obs;
  RxList<Product>? checkoutProduct = <Product>[].obs;
  RxInt subKategori = 0.obs;
  Rx<Product> p = Product().obs;
  RxInt? subPrice = 0.obs;
  RxList<bool?>? isChecked = <bool>[].obs;
  RxList<String>? itemCodeList = <String>[].obs;
  RxInt? count = 0.obs;
  RxList<int>? countId = <int>[].obs;
  RxList<qtyTotal>? qt = <qtyTotal>[].obs;
  RxList<subT>? st = <subT>[].obs;
  RxList<String>? nameP = <String>[].obs;
  RxList<String>? location = <String>[].obs;
  RxList<String>? kategori = <String>[].obs;
  RxList<String>? type = <String>[].obs;
  RxList<Uint8List>? img = <Uint8List>[].obs;
  RxList<int>? menit = <int>[].obs;
  RxList<int>? harga = <int>[].obs;
  RxList<HPP>? hpp = <HPP>[].obs;

  RxInt bottomNavIndex = 0.obs;
  @override
  void onInit() {
    super.onInit();
    getProduct();
  }

 

  Future getProduct() async {
    DbManager db = DbManager();
    listProduct!.clear();
    listProduct!.value = await db.getProduct();
    print(listProduct.toString());
    for (int i = 0; i < listProduct!.length; i++) {
      isChecked!.add(false);
      itemCodeList!.add(listProduct![i].item_code!);
      countId!.add(count!.value);
      img!.add(listProduct![i].img!);
      hpp!.add(HPP(
          hpp: double.parse(listProduct![i].hrg_beli!).toInt().obs,
          id: int.parse(listProduct![i].id!.toString()).obs));
      harga!.add(double.parse(listProduct![i].hrg_jual!).toInt());
      nameP!.add(listProduct![i].item_name!);

      type!.add(listProduct![i].type!);
      location!.add(listProduct![i].location!);
      menit!.add(listProduct![i].minute!);
      kategori!.add(listProduct![i].kategori ?? '');
      qt!.add(qtyTotal(
          id: int.parse(listProduct![i].id!.toString()).obs, qty: 1.obs));
      st!.add(subT(
          id: int.parse(listProduct![i].id!.toString()).obs,
          subTotal:
              double.parse(listProduct![i].hrg_jual.toString()).toInt().obs));
    }
  }

  Future setPay() async {
    checkoutProduct!.clear();
    if (checkoutProduct != null) {
      for (int i = 0; i < product!.length; i++) {
        checkoutProduct!.add(
          Product(
              hrg_beli: hpp![i].hpp.toString(),
              hrg_jual: harga![i].toString(),
               item_name: nameP![i],
              item_code: itemCodeList![i],
              qty: qt![i].qty!.value,
              sub_total: st![i].subTotal!.value,
              kategori: kategori![i],
              id: st![i].id!.value,
              img: img![i],
              location: location![i],
              minute: menit![i],
              type: type![i]),
        );
      }
      Get.to(CheckoutPage(checkoutProduct));
    }
  }

  void setTotal1(int index2) {
    if (isChecked![index2] == false) {
      product!.add(listProduct![index2]);
      count!.value = count!.value + 1;
      setChecked(true, index2);
      itemCodeList!.add(listProduct![index2].item_code!);
      countId!.add(listProduct![index2].id!);
      img!.add(listProduct![index2].img!);
      hpp!.add(HPP(
          hpp: double.parse(listProduct![index2].hrg_beli!).toInt().obs,
          id: int.parse(listProduct![index2].id!.toString()).obs));
      harga!.add(double.parse(listProduct![index2].hrg_jual!).toInt());
     
      nameP!.add(listProduct![index2].item_name!);

      type!.add(listProduct![index2].type!);
      location!.add(listProduct![index2].location!);
      menit!.add(listProduct![index2].minute!);
      kategori!.add(listProduct![index2].kategori ?? '');
       qt!.remove(qtyTotal(
          id: int.parse(listProduct![index2].id!.toString()).obs, qty: 1.obs));
      qt!.add(qtyTotal(
          id: int.parse(listProduct![index2].id!.toString()).obs, qty: 1.obs));
      st!.remove(subT(
          id: int.parse(listProduct![index2].id!.toString()).obs,
          subTotal:
              double.parse(listProduct![index2].hrg_jual.toString()).toInt().obs));
      st!.add(subT(
          id: int.parse(listProduct![index2].id!.toString()).obs,
          subTotal: double.parse(listProduct![index2].hrg_jual.toString())
              .toInt()
              .obs));
      addSubPrice(double.parse(listProduct![index2].hrg_jual!).toInt() * 1);
      p.value.qty = p.value.qty! + 1;
    } else if (isChecked![index2] == true) {
      setChecked(false, index2);
      product!.remove(listProduct![index2]);
      count!.value = count!.value - 1;
      minSubPrice2(((st![index2].subTotal!.value) ));
      minProduct(index2);
      type!.remove(listProduct![index2].type!);
      location!.remove(listProduct![index2].location!);
      img!.remove(listProduct![index2].img);
      menit!.remove(listProduct![index2].minute);
      kategori!.remove(listProduct![index2].kategori!);
      itemCodeList!.remove(listProduct![index2].item_code);
      countId!.remove(listProduct![index2].id!);
      hpp!.remove(HPP(
          hpp: double.parse(listProduct![index2].hrg_beli!).toInt().obs,
          id: int.parse(listProduct![index2].id!.toString()).obs));
      harga!.remove(double.parse(listProduct![index2].hrg_jual!).toInt());
      nameP!.remove(listProduct![index2].item_name!);
      // qt!.removeAt(index2);
      // st!.removeAt(index2);
      // p.value.qty = p.value.qty! - 1;

    }
  }

  void decreaseProduct(int? index) {
    if (qt![index!].qty!.value != 1) {
      minSubPrice(harga![index] * 1);
      qt!.forEach((element) {
        if (element.id!.value ==
            int.parse(listProduct![index].id!.toString())) {
          element.qty!.value = element.qty!.value - 1;
        }
      });
      st!.forEach((element) {
        if (element.id!.value ==
            int.parse(listProduct![index].id!.toString())) {
          element.subTotal!.value =
              int.parse(element.subTotal!.value.toString()) -
                  double.parse(listProduct![index].sub_total.toString())
                      .toInt();
        }
      });
    }
  }

  void sumProduct(int? index) {
    qt!.forEach((element) {
      if (element.id!.value == int.parse(listProduct![index!].id!.toString())) {
        element.qty!.value = element.qty!.value + 1;
      }
    });
    st!.forEach((element) {
      if (element.id!.value == int.parse(listProduct![index!].id!.toString())) {
        element.subTotal!.value =
            int.parse(element.subTotal!.value.toString()) +
                double.parse(listProduct![index].sub_total.toString()).toInt();
      }
    });
  }

  void minProduct(int? index) {
    qt!.forEach((element) {
      if (element.id!.value == int.parse(listProduct![index!].id!.toString())) {
        element.qty!.value = 1;
      }
    });
    st!.forEach((element) {
      if (element.id!.value == int.parse(listProduct![index!].id!.toString())) {
        element.subTotal!.value =
            double.parse(harga![index].toString()).toInt();
      }
    });
  }

  void minSubPrice(int k) {
    subPrice = subPrice! - k;
  }

  void minSubPrice2(int k) {
    subPrice!.value = subPrice!.value - k;
  }

  void setChecked(bool? value, int index) {
    isChecked![index] = value;
  }

  void addSubPrice(int k) {
    subPrice!.value = subPrice!.value + k;
  }

  void setSubKategori(int k) {
    subKategori.value = k;
  }
}

class qtyTotal {
  RxInt? qty;
  RxInt? id;
  qtyTotal({this.qty, this.id});
}

class subT {
  RxInt? subTotal = 0.obs;
  RxInt? id;
  subT({this.subTotal, this.id});
}

class HPP {
  RxInt? hpp;
  RxInt? id;
  HPP({this.hpp, this.id});
}
