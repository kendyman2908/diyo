import 'package:diyo/app/model/assets_color.dart';
import 'package:flutter/material.dart';
import 'package:animated_bottom_navigation_bar/animated_bottom_navigation_bar.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../menu/controller/display_menu_controller.dart';
import '../../menu/views/add_menu.dart';
import '../../menu/views/display_menu.dart';
import '../../scanQRtable/views/scan_table.dart';

class Home extends StatelessWidget {
  Home({Key? key}) : super(key: key);
  List<IconData> iconList = [
    Icons.home,
    Icons.access_time_filled,
    Icons.favorite,
    Icons.person
  ];
  @override
  Widget build(BuildContext context) {
    bool isKeyboardOpen = MediaQuery.of(context).viewInsets.bottom != 0.0;
    ScreenUtil.init(context, designSize: const Size(360, 720));
    return GetBuilder<DisplayMenuController>(
        init: DisplayMenuController(),
        builder: (controller) {
          return Obx(() => Scaffold(
                floatingActionButton: isKeyboardOpen
                    ? null
                    : FloatingActionButton(
                        onPressed: () {
                          Get.to(ScanQRTable());
                        },
                        backgroundColor: const Color(0xffF1523F),
                        child: const Icon(
                          Icons.qr_code_scanner_outlined,
                          color: Colors.white,
                        ),
                      ),
                appBar: AppBar(
                  backgroundColor: AssetColors.primary,
                  title: const Text("DIYO"),
                ),
                floatingActionButtonLocation:
                    FloatingActionButtonLocation.centerDocked,
                body: DisplayMenu(),
                bottomNavigationBar: AnimatedBottomNavigationBar(
                  icons: iconList,
                  activeColor: AssetColors.primary,
                  inactiveColor: AssetColors.inActive,
                  activeIndex: controller.bottomNavIndex.value,
                  gapLocation: GapLocation.center,
                  notchSmoothness: NotchSmoothness.softEdge,
                  onTap: (index) => controller.bottomNavIndex.value = index,
                  //other params
                ),
              ));
        });
  }
}
