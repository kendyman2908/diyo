import 'dart:async';
import 'dart:ui' as ui;

import 'package:diyo/app/model/product.dart';
import 'package:diyo/app/screens/order/views/order.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:qr_code_dart_scan/qr_code_dart_scan.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../menu/controller/display_menu_controller.dart';

class ScanQRTable extends StatefulWidget {
  static const routeName = '/live';

  @override
  _LiveDecodePageState createState() => _LiveDecodePageState();
}

class _LiveDecodePageState extends State<ScanQRTable> {
  Result? currentResult;

  @override
  Widget build(BuildContext context) {
    DisplayMenuController controller = Get.put(DisplayMenuController());
    ScreenUtil.init(context, designSize: const Size(360, 720));
    return Scaffold(
      body: Stack(
        children: [
          QRCodeDartScanView(
            resolutionPreset: QRCodeDartScanResolutionPreset.medium,
            scanInvertedQRCode: false,
            typeCamera: TypeCamera.back,
            typeScan: TypeScan.takePicture,
            onCapture: (Result result) {
              setState(() {
                currentResult = result;
              });
              if (currentResult!.text == 'meja001212') {
                // controller.setToOrder(widget.product!);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => OrderPage()));
              }
            },
          ),
          Positioned(
              left: 100.w,
              right: 100.w,
              top: 200.h,
              child: Image.asset('assets/images/scan.png'))
        ],
      ),
    );
  }
}
