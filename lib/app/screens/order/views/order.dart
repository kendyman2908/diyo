import 'package:diyo/app/model/assets_color.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../model/product.dart';
import '../../menu/controller/display_menu_controller.dart';

class OrderPage extends GetView<DisplayMenuController> {
  final _key = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, designSize: const Size(360, 720));
    return GetBuilder<DisplayMenuController>(
        init: DisplayMenuController(),
        builder: (controller) {
          return Obx(() => Scaffold(
              resizeToAvoidBottomInset: false,
              extendBodyBehindAppBar: true,
              appBar: AppBar(
                elevation: 0,
                leading: SizedBox(
                  width: 25.w,
                  height: 25.h,
                  child: ElevatedButton(
                      style: ButtonStyle(
                          padding: MaterialStateProperty.all<EdgeInsets>(
                              EdgeInsets.zero),
                          foregroundColor:
                              MaterialStateProperty.all<Color>(Colors.white),
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Colors.transparent),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50.r),
                          ))),
                      onPressed: () {
                        Get.back();
                      },
                      child: Container(
                        width: 25.w,
                        height: 25.h,
                        decoration: BoxDecoration(
                            color: AssetColors.primary,
                            borderRadius: BorderRadius.circular(50.r)),
                        child: Center(
                            child: Padding(
                          padding: EdgeInsets.only(left: 4.w),
                          child: Icon(
                            Icons.arrow_back_ios,
                            color: Colors.white,
                            size: 14.sp,
                          ),
                        )),
                      )),
                ),
                backgroundColor: Colors.transparent,
              ),
              floatingActionButton: controller.count!.value == 0
                  ? const SizedBox()
                  : ElevatedButton(
                      style: ButtonStyle(
                          padding: MaterialStateProperty.all<EdgeInsets>(
                              EdgeInsets.zero),
                          foregroundColor:
                              MaterialStateProperty.all<Color>(Colors.white),
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Colors.transparent),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50.r),
                          ))),
                      onPressed: () {
                        controller.setPay();
                      },
                      child: Container(
                          width: 1.sw - 30.w,
                          height: 45.h,
                          decoration: BoxDecoration(
                            color: AssetColors.primary,
                            borderRadius: BorderRadius.circular(
                              8,
                            ),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text(
                                "${controller.count}",
                                style: GoogleFonts.poppins(
                                    fontSize: 14,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700),
                              ),
                              Text(
                                "View Basket",
                                style: GoogleFonts.poppins(
                                    fontSize: 14,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700),
                              ),
                              Text(
                                "${controller.subPrice}",
                                style: GoogleFonts.poppins(
                                    fontSize: 14,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700),
                              )
                            ],
                          )),
                    ),
              backgroundColor:Colors.white,
              body: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Stack(
                      clipBehavior: Clip.none,
                      children: [
                        Container(
                          margin: EdgeInsets.only(bottom: 25.h),
                          height: 200.h,
                          decoration: const BoxDecoration(color: Colors.white,
                              image: DecorationImage(
                                  image:
                                      AssetImage('assets/images/seefood.jpg'),
                                  fit: BoxFit.fill)),
                        ),
                        Positioned(
                            bottom: 40.h,
                            left: 20.w,
                            child: Container(
                              width: 100.w,
                              height: 25.h,
                              decoration: BoxDecoration(
                                  color: AssetColors.primary,
                                  borderRadius: BorderRadius.circular(4.r)),
                              child: Center(
                                  child: Text(
                                "No Meja 01",
                                style: GoogleFonts.poppins(
                                  color: Colors.white,
                                  fontSize: 12.sp,
                                ),
                              )),
                            )),
                        Positioned(bottom: 10,right: 25.w,
                            child: SizedBox(
                          width: 45.w,
                          height: 45.h,
                          child: ElevatedButton(
                              style: ButtonStyle(
                                  padding:
                                      MaterialStateProperty.all<EdgeInsets>(
                                          EdgeInsets.zero),
                                  foregroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.white),
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Colors.white),
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(50.r),
                                  ))),
                              onPressed: () {},
                              child: Center(
                                  child: Padding(
                                padding: EdgeInsets.only(left: 0.w),
                                child: Icon(
                                  Icons.favorite_outline,
                                  color: AssetColors.primary,
                                  size: 26.sp,
                                ),
                              ))),
                        ))
                      ],
                    ),
                    Container(
                      width: 1.sw,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.r)),
                      child: Padding(
                        padding:  EdgeInsets.symmetric(horizontal: 16.w,vertical: 4.h),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "dealJava",
                              style: GoogleFonts.poppins(
                                  color: Colors.black87,
                                  fontSize: 18.sp,
                                  fontWeight: FontWeight.w600),
                            ),
                            SizedBox(
                              height: 4.h,
                            ),
                            Text(
                              "Kopi",
                              style: GoogleFonts.poppins(
                                  color: Colors.black54,
                                  fontSize: 12.sp,
                                  fontWeight: FontWeight.w400),
                            ),
                            SizedBox(
                              height: 4.h,
                            ),
                            Text(
                              "Jalan Mayang No. 24 Medan",
                              style: GoogleFonts.poppins(
                                  color: Colors.black54,
                                  fontSize: 12.sp,
                                  fontWeight: FontWeight.w400),
                            ),
                            Container(
                              height: 1.5.h,
                              width: 1.sw,
                              color: Colors.grey,
                            ),
                            SizedBox(
                              height: 8.h,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text("Buka until 19.00 today",
                                    style: GoogleFonts.poppins(
                                        color: AssetColors.primary,
                                        fontSize: 12.sp,
                                        fontWeight: FontWeight.w400)),
                                Container(
                                  width: 100.w,
                                  height: 20.h,
                                  decoration: BoxDecoration(
                                      color: AssetColors.primary,
                                      borderRadius: BorderRadius.circular(4.r)),
                                  child: Center(
                                      child: Text(
                                    "1096798 km",
                                    style: GoogleFonts.poppins(
                                      color: Colors.white,
                                      fontSize: 12.sp,
                                    ),
                                  )),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 18.h,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10.r),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: 1.sw,
                            height: 375.h,
                            child: ListView.builder(
                                padding: EdgeInsets.only(bottom: 100.h),
                                itemCount: controller.listProduct!.length,
                                itemBuilder: (context, index) {
                                  Product e = controller.listProduct![index];

                                  return Obx(() => GestureDetector(
                                        onTap: () {
                                          controller.setSubKategori(index);

                                          controller.setTotal1(index);
                                        },
                                        child: Container(
                                          padding: EdgeInsets.all(10.sp),
                                          margin: EdgeInsets.symmetric(
                                              vertical: 4.h),
                                          decoration: BoxDecoration(
                                              color:
                                                  controller.isChecked![index]!
                                                      ? AssetColors.primary
                                                      : Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(8.r)),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8.0),
                                                    child: Image.memory(
                                                      e.img!,
                                                      width: 65.w,
                                                      height: 65.h,
                                                      fit: BoxFit.cover,
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 8.w,
                                                  ),
                                                  SizedBox(
                                                    width: 100.w,
                                                    height: 65.h,
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceEvenly,
                                                      children: [
                                                        Text(
                                                          e.item_name!,
                                                          style: GoogleFonts
                                                              .poppins(
                                                            fontSize: 12.sp,
                                                            fontWeight:
                                                                FontWeight.w600,
                                                          ),
                                                        ),
                                                        Text(
                                                          e.location!,
                                                          style: GoogleFonts
                                                              .poppins(
                                                            fontSize: 12.sp,
                                                            fontWeight:
                                                                FontWeight.w300,
                                                          ),
                                                        ),
                                                        Text(
                                                          e.type!,
                                                          style: GoogleFonts
                                                              .poppins(
                                                            fontSize: 12.sp,
                                                            fontWeight:
                                                                FontWeight.w400,
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                  controller.isChecked![index]!
                                                      ? SizedBox(
                                                          child: Row(
                                                            children: <Widget>[
                                                              SizedBox(
                                                                child: Row(
                                                                  children: <
                                                                      Widget>[
                                                                    InkWell(
                                                                      onTap:
                                                                          () {
                                                                        controller
                                                                            .decreaseProduct(index);
                                                                      },
                                                                      child:
                                                                          Container(
                                                                        decoration: BoxDecoration(
                                                                            color: controller.isChecked![index]!
                                                                                ? Colors.white
                                                                                : AssetColors.primary,
                                                                            borderRadius: BorderRadius.circular(4.r)),
                                                                        width:
                                                                            24.w,
                                                                        height:
                                                                            24.h,
                                                                        child:
                                                                            Icon(
                                                                          Icons
                                                                              .remove,
                                                                          color:
                                                                              AssetColors.primary,
                                                                          size:
                                                                              24.sp,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    SizedBox(
                                                                      width:
                                                                          24.w,
                                                                      child: Center(
                                                                          child: Text(
                                                                        '${controller.qt![index].qty}x',
                                                                        style: GoogleFonts.poppins(
                                                                            color: Colors
                                                                                .white,
                                                                            fontSize: 14
                                                                                .sp,
                                                                            height:
                                                                                1,
                                                                            fontWeight:
                                                                                FontWeight.w300),
                                                                      )),
                                                                    ),
                                                                    InkWell(
                                                                      onTap:
                                                                          () {
                                                                        controller.addSubPrice(
                                                                            controller.harga![index] *
                                                                                1);

                                                                        controller
                                                                            .sumProduct(index);
                                                                      },
                                                                      child: Container(
                                                                          decoration: BoxDecoration(color: controller.isChecked![index]! ? Colors.white : AssetColors.primary, borderRadius: BorderRadius.circular(4.r)),
                                                                          width: 24.w,
                                                                          height: 24.h,
                                                                          child: RichText(
                                                                            text:
                                                                                TextSpan(children: [
                                                                              WidgetSpan(
                                                                                child: Icon(Icons.add, color: AssetColors.primary, size: 24.sp),
                                                                              ),
                                                                            ], style: GoogleFonts.poppins(fontWeight: FontWeight.w900)),
                                                                          )),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        )
                                                      : const SizedBox(),
                                                ],
                                              ),
                                              Text(
                                                (controller.st![index].subTotal
                                                            ?.value ??
                                                        0)
                                                    .toString(),
                                                style: GoogleFonts.poppins(
                                                  fontSize: 12.sp,
                                                  fontWeight: FontWeight.w400,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ));
                                }),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              )));
        });
  }
}
