import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../../model/assets_color.dart';
import '../../../model/product.dart';
import '../controller/checkout_controller.dart';

class CheckoutPage extends GetView<CheckoutController> {
  RxList<Product>? checkoutList;
  CheckoutPage(this.checkoutList);
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, designSize: const Size(360, 720));
    return GetBuilder<CheckoutController>(
        init: CheckoutController(checkoutList),
        builder: (controller) {
          return Obx(() => Scaffold(
                appBar: AppBar(
                  backgroundColor: AssetColors.primary,
                  title: const Text("MEJA 00-1212"),
                ),
                floatingActionButton: ElevatedButton(
                  style: ButtonStyle(
                      padding: MaterialStateProperty.all<EdgeInsets>(
                          EdgeInsets.zero),
                      foregroundColor:
                          MaterialStateProperty.all<Color>(Colors.white),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.transparent),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50.r),
                      ))),
                  onPressed: () {},
                  child: Container(
                      width: 1.sw - 30.w,
                      height: 45.h,
                      decoration: BoxDecoration(
                        color: AssetColors.primary,
                        borderRadius: BorderRadius.circular(
                          8,
                        ),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            "${controller.checkoutList!.length}",
                            style: GoogleFonts.poppins(
                                fontSize: 14,
                                color: Colors.white,
                                fontWeight: FontWeight.w700),
                          ),
                          Text(
                            "Checkout",
                            style: GoogleFonts.poppins(
                                fontSize: 14,
                                color: Colors.white,
                                fontWeight: FontWeight.w700),
                          ),
                          Text(
                            "${controller.resultCheckout}",
                            style: GoogleFonts.poppins(
                                fontSize: 14,
                                color: Colors.white,
                                fontWeight: FontWeight.w700),
                          )
                        ],
                      )),
                ),
                body: SingleChildScrollView(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 8.h,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 16.w),
                          child: Text(
                            "PesananKu",
                            style: GoogleFonts.poppins(
                                fontSize: 18,
                                fontWeight: FontWeight.w700,
                                color: Colors.black87),
                          ),
                        ),
                        ListView.builder(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            padding: EdgeInsets.zero,
                            itemCount: controller.checkoutList!.length,
                            itemBuilder: (context, index) {
                              Product data = controller.checkoutList![index];

                              return Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 16.w, vertical: 8.h),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        Container(
                                          width: 40.w,
                                          height: 22.h,
                                          decoration: BoxDecoration(
                                              color: AssetColors.primary,
                                              borderRadius:
                                                  BorderRadius.circular(4.r)),
                                          child: Center(
                                              child: Text(
                                            "${data.qty}X",
                                            style: GoogleFonts.poppins(
                                                fontSize: 14,
                                                color: Colors.white,
                                                fontWeight: FontWeight.w500),
                                          )),
                                        ),
                                        SizedBox(
                                          width: 8.w,
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              data.item_name!,
                                              style: GoogleFonts.poppins(
                                                fontSize: 12.sp,
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                            Text(
                                              data.type!,
                                              style: GoogleFonts.poppins(
                                                fontSize: 12.sp,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          data.sub_total!.toString(),
                                          style: GoogleFonts.poppins(
                                            fontSize: 12.sp,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                        SizedBox(width: 16.h),
                                        GestureDetector(
                                          onTap: () {
                                            controller.checkoutList!
                                                .removeAt(index);
                                            checkoutList?.forEach((e) {
                                              controller.resultCheckout.value =
                                                  controller.resultCheckout
                                                          .value -
                                                     ( data.sub_total!);
                                            });
                                          },
                                          child: Icon(
                                            Icons
                                                .remove_circle_outline_outlined,
                                            color: AssetColors.primary,
                                            size: 22,
                                          ),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              );
                            }),
                        Container(
                            margin: EdgeInsets.symmetric(horizontal: 16.w),
                            height: 1,
                            width: 1.sw,
                            color: Colors.grey),
                        Padding(
                          padding: EdgeInsets.all(16.sp),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Subtotal",
                                  style: GoogleFonts.poppins(
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500,
                                  )),
                              Text("${controller.resultCheckout}",
                                  style: GoogleFonts.poppins(
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500,
                                  )),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 8.h,
                        ),
                        Container(
                            margin: EdgeInsets.symmetric(horizontal: 16.w),
                            height: 10.h,
                            width: 1.sw,
                            color: Colors.grey),
                        Padding(
                          padding: EdgeInsets.all(16.sp),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Subtotal",
                                  style: GoogleFonts.poppins(
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500,
                                  )),
                              Text("${controller.resultCheckout}",
                                  style: GoogleFonts.poppins(
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500,
                                  )),
                            ],
                          ),
                        )
                      ]),
                ),
              ));
        });
  }
}
