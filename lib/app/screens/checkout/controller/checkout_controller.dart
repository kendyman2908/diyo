import 'package:get/get.dart';

import '../../../model/product.dart';

class CheckoutController extends GetxController {
  RxList<Product>? checkoutList;
  CheckoutController(this.checkoutList);

  RxInt resultCheckout = 0.obs;
  Future getData() async {
    checkoutList!.forEach((e) {
      resultCheckout.value = resultCheckout.value + e.sub_total!;
    });
  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    getData();
  }
}
